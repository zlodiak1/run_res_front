import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RegistrationService } from '../../services/registration.service';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  
  private form: FormGroup;

  constructor(private registrationService: RegistrationService) { }

  ngOnInit() {
    this.form = new FormGroup({
      'email':     new FormControl(null, [Validators.required, Validators.email]),
      'username':  new FormControl(null, [Validators.required, Validators.required]),
      'password1': new FormControl(null, [Validators.required, Validators.minLength(6)]),
      'password2': new FormControl(null, [Validators.required, Validators.minLength(6)]),
    });

    this.registrationService.getCSRF().subscribe(
      next => console.log(next),
      error => console.log(error)
    );
  }

  private submit() {
    const email = this.form.value.email;
    const username = this.form.value.username;
    const password = this.form.value.password1;
    this.registrationService.registrate(email, username, password).subscribe(
      next => console.log(next),
      error => console.log(error)
    );
  }

}
