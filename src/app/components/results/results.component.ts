import { Component, OnInit } from '@angular/core';
import { ResultsService } from '../../services/results.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  constructor(private resultsService: ResultsService) { }

  ngOnInit() {
  }

  private getResults() {
    this.resultsService.getResults().subscribe(
      results => {
        console.log(results)
      },
      error => {
        console.log('get results is failed!', error);
      }
    );
  }

}
