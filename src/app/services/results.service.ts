import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';


interface Result {
  model: string;
  pk: number;
  fields: Details;
}

interface Details {
  user: number;
  period_min: number;
  distance_km: number;
  desc: string;
  created_date: string;
  published_date: string;
}


@Injectable({
  providedIn: 'root'
})
export class ResultsService {

  constructor(private httpClient: HttpClient) { }

  getResults() {
    const resultsUrl = 'http://127.0.0.1:8000/result/2019-06-23T10:56:23Z/2019-06-24T10:56:23Z/3';
    return this.httpClient.get<Result[]>(resultsUrl);
  }

}
