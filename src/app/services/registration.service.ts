import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private httpClient: HttpClient) { }

  registrate(
    username: string,
    email: string,
    password: string
  ) {
    const postParams = { username, email, password }
    console.log(postParams)
    const registrationUrl = 'http://127.0.0.1:8000/app_auth/registration';
    return this.httpClient.post<any[]>(registrationUrl, postParams);
  }

  getCSRF() {
    console.log('----getCSRF')
    const resultsUrl = 'http://127.0.0.1:8000/app_auth/get_csrf';
    return this.httpClient.get<any[]>(resultsUrl);    
  }

}
